﻿using UnityEngine;
using System.Collections;

public class ManagerMenu : MonoBehaviour {

	//start button
	public GameObject startButton;
	private bool startGame = false;

	//player selection
	public GameObject[] playerNum;

	void Start(){
		StartCoroutine("pressStartButton");
	}

	void Update () {
		if(startGame){
			iTween.ScaleAdd(startButton.gameObject, iTween.Hash("x", 1.5f,
														 		"y", 1.5f,
															 	"time", .5f,
														 		"looptype", "pingPong"));
			if(Input.GetKeyDown(KeyCode.Return)){		
				startButton.SetActive(false);

				for(int i = 0; i < playerNum.Length; i++){
					playerNum[i].SetActive(true);
				}
			}
		}
	}

	IEnumerator pressStartButton(){
		yield return new WaitForSeconds(.5f);
		startGame = true;

		startButton.SetActive(true);
	}

	//button functios
	public void singlePlayerGame(){
		Application.LoadLevel(1);
	}

	public void twoPlayersGame(){
		Application.LoadLevel(2);
	}
}
