﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class KeyboardMenu : MonoBehaviour {

	public Button buttonSinglePlayer;
	public Button buttonTwoPlayers;

	int i = 0;

	public Text zenModeText;
	public Text hardModeText;

	void Update()
	{
		var pointer = new PointerEventData(EventSystem.current); // pointer event for Execute
		if (Input.GetKeyDown(KeyCode.DownArrow)) // force hover
		{
			i ++;
			if(i == 1){
				ExecuteEvents.Execute(buttonTwoPlayers.gameObject, pointer, ExecuteEvents.pointerExitHandler);
				ExecuteEvents.Execute(buttonSinglePlayer.gameObject, pointer, ExecuteEvents.pointerEnterHandler);

				hardModeText.color = Color.white;
				zenModeText.color = Color.yellow;
			}

			if(i == 2){
				ExecuteEvents.Execute(buttonSinglePlayer.gameObject, pointer, ExecuteEvents.pointerExitHandler);
				ExecuteEvents.Execute(buttonTwoPlayers.gameObject, pointer, ExecuteEvents.pointerEnterHandler);

				zenModeText.color = Color.white;
				hardModeText.color = Color.yellow;
			}

			if (i > 2){
				i = 0;
			}
		}

		if (Input.GetKeyDown(KeyCode.UpArrow)) // force hover
		{
			i --;
			if(i == 1){
				ExecuteEvents.Execute(buttonTwoPlayers.gameObject, pointer, ExecuteEvents.pointerExitHandler);
				ExecuteEvents.Execute(buttonSinglePlayer.gameObject, pointer, ExecuteEvents.pointerEnterHandler);

				hardModeText.color = Color.white;
				zenModeText.color = Color.yellow;
			}

			if(i == 2){
				ExecuteEvents.Execute(buttonSinglePlayer.gameObject, pointer, ExecuteEvents.pointerExitHandler);
				ExecuteEvents.Execute(buttonTwoPlayers.gameObject, pointer, ExecuteEvents.pointerEnterHandler);

				zenModeText.color = Color.white;
				hardModeText.color = Color.yellow;
			}

			if (i < 0){
				i = 0;
			}
		}

		if (Input.GetKeyDown(KeyCode.Return)) // submit (~click)
		{
			if(i == 1){
				ExecuteEvents.Execute(buttonSinglePlayer.gameObject, pointer, ExecuteEvents.submitHandler);
				i = 0;
			}

			if(i == 2){
				ExecuteEvents.Execute(buttonTwoPlayers.gameObject, pointer, ExecuteEvents.submitHandler);
				i = 0;
			}
		}

//		if (Input.GetKeyDown(KeyCode.H)) // force hover
//		{
//			ExecuteEvents.Execute(button.gameObject, pointer, ExecuteEvents.pointerEnterHandler);
//		}
//		if (Input.GetKeyDown(KeyCode.U)) // un-hover (end hovering)
//		{
//			ExecuteEvents.Execute(buttonSinglePlayer.gameObject, pointer, ExecuteEvents.pointerExitHandler);
//		}
//		if (Input.GetKeyDown(KeyCode.S)) // submit (~click)
//		{
//			ExecuteEvents.Execute(buttonSinglePlayer.gameObject, pointer, ExecuteEvents.submitHandler);
//		}
//		if (Input.GetKeyDown(KeyCode.P)) // down: press
//		{
//			ExecuteEvents.Execute(buttonSinglePlayer.gameObject, pointer, ExecuteEvents.pointerDownHandler);
//		}
//		if (Input.GetKeyUp(KeyCode.P)) // up: release
//		{
//			ExecuteEvents.Execute(buttonSinglePlayer.gameObject, pointer, ExecuteEvents.pointerUpHandler);
//		}

	}
}
