﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerOne : MonoBehaviour {

	//board
	public int [,] board;

	//block
	public Transform PinkBlock;
	public Transform GreenBlock;
	public Transform BlueBlock;
	public Transform RedBlock;
	public Transform CyanBlock;
	public Transform DarkGreenBlock;
	public Transform VioletBblock;

	public Transform RockBlock;

	//spawn boolean
	public bool spawn = true;

	//seconds before next block spawn
	public float nextBlockSpawnTime = .5f;

	//block fall speed
	public float blockFallSpeed = .5f;

	//game over lvl //20 board + 2 edge
	public int gameOverHeight = 22;

	//current spawned shapes
	private List<Transform> shapes = new List<Transform>();

	private List<Transform> rockShapes = new List<Transform>();

	//set true if game over
	private bool gameOver;
	public GameObject gameOverText;
	public GameObject winPlayerOne;

	//current rotation of an object
	private int currentRot = 0;

	//current pivot of the shape
	private GameObject pivot;

	private GameObject pivotRow;

	//score
	public int score = 0;
	public Text scoreText;

	//lines
	public int lines = 0;
	public Text linesText;

	//level
	public int level = 1;
	public Text levelText;

	//image next shape
	public GameObject IShape;
	public GameObject JShape;
	public GameObject LShape;
	public GameObject OShape;
	public GameObject SShape;
	public GameObject TShape;
	public GameObject ZShape;

	//next random shape
	private int nextShape;

	//tetris mode
	private bool zenTetrisBool = false;
	private bool hardTetrisBool = false;

	//interfaz
	public GameObject scoreBacground;
	public GameObject levelBackground;
	public GameObject linesBacground;
	public GameObject nextText;

	public Material edgeMaterial;
	public Material boardMat;

	//Player One messages
	public GameObject playerOne;
	private int combo = 0;

	void Start () {
		//default board 10x16: 1+10+1 side edge, +2 spawn space, +1 top edge, 20 height, +1 down edge
		board = new int[40,24];

		//generate board
		GenBoard();
	}

	void checkFallSpeed(){
		InvokeRepeating("moveDown", blockFallSpeed, blockFallSpeed);
	}


	#region generate boards
	//create a board PlayerOne
	void GenBoard(){
		for (int x = 0; x < board.GetLength(0); x++){
			for(int y = 0; y < board.GetLongLength(1); y++){
				if (x < 29 && x > 18){
					if(y > 0 && y < board.GetLength(1) - 2){
						//board
						board[x,y] = 0;
						GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
						cube.transform.position = new Vector3(x, y, 1);
						Material material = new Material(Shader.Find("Diffuse"));
						material.color = Color.blue;
						cube.GetComponent<Renderer>().material = boardMat;
						cube.transform.parent = transform;
					}
					else if(y < board.GetLength(1) - 2){
						board[x,y] = 1;
						GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
						cube.transform.position = new Vector3 (x, y, 0);
						Material material = new Material(Shader.Find("Diffuse"));
						material.color = Color.black;
						cube.GetComponent<Renderer>().material = edgeMaterial;
						cube.transform.parent = transform;
						cube.GetComponent<Collider>().isTrigger = true;
					}
				}
				else if(y < board.GetLength(1) - 2){
					//left and right edge
					board[x,y] = 1;
					GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
					cube.transform.position = new Vector3 (18, y, 0);
					Material material = new Material(Shader.Find("Diffuse"));
					material.color = Color.black;
					cube.GetComponent<Renderer>().material = edgeMaterial;
					cube.transform.parent = transform;
				}
			}
		}

		for (int x = 0; x < board.GetLength(0); x++){
			for(int y = 0; y < board.GetLongLength(1); y++){
				if(y < board.GetLength(1) - 2){
					GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
					cube.transform.position = new Vector3 (29, y, 0);
					Material material = new Material(Shader.Find("Diffuse"));
					material.color = Color.black;
					cube.GetComponent<Renderer>().material = edgeMaterial;
					cube.transform.parent = transform;
				}
			}
		}
	}

	#endregion 

	//create a block at the position PlayerOne
	Transform GenPinkBlock (Vector3 pos){

		Transform obj = (Transform)Instantiate(PinkBlock.transform, pos, Quaternion.identity) as Transform; 
		obj.tag = "BlockTwo";

		return obj;
	}

	Transform GenGreenBlock (Vector3 pos){

		Transform obj = (Transform)Instantiate(GreenBlock.transform, pos, Quaternion.identity) as Transform; 
		obj.tag = "BlockTwo";

		return obj;
	} 

	Transform GenBlueBlock (Vector3 pos){

		Transform obj = (Transform)Instantiate(BlueBlock.transform, pos, Quaternion.identity) as Transform; 
		obj.tag = "BlockTwo";

		return obj;
	} 

	Transform GenRedBlock (Vector3 pos){

		Transform obj = (Transform)Instantiate(RedBlock.transform, pos, Quaternion.identity) as Transform; 
		obj.tag = "BlockTwo";

		return obj;
	} 

	Transform GenCyanBlock (Vector3 pos){

		Transform obj = (Transform)Instantiate(CyanBlock.transform, pos, Quaternion.identity) as Transform; 
		obj.tag = "BlockTwo";

		return obj;
	} 

	Transform GenDarkGreenBlock (Vector3 pos){

		Transform obj = (Transform)Instantiate(DarkGreenBlock.transform, pos, Quaternion.identity) as Transform; 
		obj.tag = "BlockTwo";

		return obj;
	} 

	Transform GenVioletBlock (Vector3 pos){

		Transform obj = (Transform)Instantiate(VioletBblock.transform, pos, Quaternion.identity) as Transform; 
		obj.tag = "BlockTwo";

		return obj;
	} 

	#region create shapes
	//create a shape(4 blocks S I Z T L J O) PlayerOne
	void SpawnShape(int random){

		int shape = random;
		int height = board.GetLength(1) - 4;
		int xPos = (board.GetLength(0)/2 - 1) + 6;

		//new pivot
		pivot = new GameObject("RotateAround");

		//Sshape
		if (shape == 0){
			pivot.transform.position = new Vector3 (xPos, height + 1, 0);

			shapes.Add(GenPinkBlock(new Vector3 (xPos, height, 0)));
			shapes.Add(GenPinkBlock(new Vector3 (xPos - 1, height, 0)));
			shapes.Add(GenPinkBlock(new Vector3 (xPos, height + 1, 0)));
			shapes.Add(GenPinkBlock(new Vector3 (xPos + 1, height + 1, 0)));

			Debug.Log("Spawn Sshape");
		}
		//IShape
		else if (shape == 1){
			pivot.transform.position = new Vector3(xPos + .5f, height + 1.5f, 0);


			shapes.Add(GenGreenBlock(new Vector3 (xPos, height, 0)));
			shapes.Add(GenGreenBlock(new Vector3 (xPos, height + 1, 0)));
			shapes.Add(GenGreenBlock(new Vector3 (xPos, height + 2, 0)));
			shapes.Add(GenGreenBlock(new Vector3 (xPos, height + 3, 0)));

			Debug.Log("Spawn Ishape");
		}
		//OShape
		else if (shape == 2){
			pivot.transform.position = new Vector3(xPos + .5f, height + .5f, 0);


			shapes.Add(GenBlueBlock(new Vector3 (xPos, height, 0)));
			shapes.Add(GenBlueBlock(new Vector3 (xPos + 1, height, 0)));
			shapes.Add(GenBlueBlock(new Vector3 (xPos, height + 1, 0)));
			shapes.Add(GenBlueBlock(new Vector3 (xPos + 1, height + 1, 0)));

			Debug.Log("Spawn Oshape");
		}
		//JShape
		else if (shape == 3){
			pivot.transform.position = new Vector3(xPos, height + 1.0f, 0);

			shapes.Add(GenRedBlock(new Vector3 (xPos, height, 0)));
			shapes.Add(GenRedBlock(new Vector3 (xPos + 1, height, 0)));
			shapes.Add(GenRedBlock(new Vector3 (xPos, height + 1, 0)));
			shapes.Add(GenRedBlock(new Vector3 (xPos, height + 2, 0)));

			Debug.Log("Spawn Jshape");
		}
		//TShape
		else if (shape == 4){
			pivot.transform.position = new Vector3(xPos, height, 0);

			shapes.Add(GenCyanBlock(new Vector3 (xPos, height, 0)));
			shapes.Add(GenCyanBlock(new Vector3 (xPos - 1, height, 0)));
			shapes.Add(GenCyanBlock(new Vector3 (xPos + 1, height, 0)));
			shapes.Add(GenCyanBlock(new Vector3 (xPos, height + 1, 0)));

			Debug.Log("Spawn Tshape");
		}
		//LShape
		else if (shape == 5){
			pivot.transform.position = new Vector3(xPos, height + 1.0f, 0);


			shapes.Add(GenDarkGreenBlock(new Vector3 (xPos, height, 0)));
			shapes.Add(GenDarkGreenBlock(new Vector3 (xPos - 1, height, 0)));
			shapes.Add(GenDarkGreenBlock(new Vector3 (xPos, height + 1, 0)));
			shapes.Add(GenDarkGreenBlock(new Vector3 (xPos, height + 2, 0)));

			Debug.Log("Spawn Lshape");
		}
		//ZShape
		else if (shape == 6){
			pivot.transform.position = new Vector3(xPos, height + 1.0f, 0);


			shapes.Add(GenVioletBlock(new Vector3 (xPos, height, 0)));
			shapes.Add(GenVioletBlock(new Vector3 (xPos + 1, height, 0)));
			shapes.Add(GenVioletBlock(new Vector3 (xPos, height + 1, 0)));
			shapes.Add(GenVioletBlock(new Vector3 (xPos - 1, height + 1, 0)));

			Debug.Log("Spawn Zshape");
		}
	}

	#endregion


	bool CheckUserMove (Vector3 a, Vector3 b, Vector3 c, Vector3 d, bool dir){
		//check if we move a block left/right will it hit something
		//left
		if(dir){
			if(board[Mathf.RoundToInt(a.x - 1), Mathf.RoundToInt(a.y)] == 1 || board[Mathf.RoundToInt(b.x - 1), Mathf.RoundToInt(b.y)] == 1
				|| board[Mathf.RoundToInt(c.x - 1), Mathf.RoundToInt(c.y)] == 1 || board[Mathf.RoundToInt(d.x - 1), Mathf.RoundToInt(d.y)] == 1){

				return false;
			}
		}
		//right
		else {
			if(board[Mathf.RoundToInt(a.x + 1), Mathf.RoundToInt(a.y)] == 1 || board[Mathf.RoundToInt(b.x + 1), Mathf.RoundToInt(b.y)] == 1
				|| board[Mathf.RoundToInt(c.x + 1), Mathf.RoundToInt(c.y)] == 1 || board[Mathf.RoundToInt(d.x + 1), Mathf.RoundToInt(d.y)] == 1){

				return false;
			}
		}

		return true;
	}

	void Update () {
		//if there is block
		if(spawn && shapes.Count == 4){
			//get spawned blocks positions
			Vector3 a = shapes[0].transform.position;
			Vector3 b = shapes[1].transform.position;
			Vector3 c = shapes[2].transform.position;
			Vector3 d = shapes[3].transform.position;

			//move left
			if(Input.GetKeyDown(KeyCode.LeftArrow)){
				//check if we can move it left
				if(CheckUserMove(a, b, c, d, true)){

					a.x -= 1;
					b.x -= 1;
					c.x -= 1;
					d.x -= 1;

					pivot.transform.position = new Vector3(pivot.transform.position.x - 1, 
															pivot.transform.position.y, 
															pivot.transform.position.z);
					shapes[0].transform.position = a;
					shapes[1].transform.position = b;
					shapes[2].transform.position = c;
					shapes[3].transform.position = d;
				}
			}

			//move right
			if(Input.GetKeyDown(KeyCode.RightArrow)){
				//check if we can move it right
				if(CheckUserMove(a, b, c, d, false)){

					a.x += 1;
					b.x += 1;
					c.x += 1;
					d.x += 1;

					pivot.transform.position = new Vector3(pivot.transform.position.x + 1, 
															pivot.transform.position.y, 
															pivot.transform.position.z);
					shapes[0].transform.position = a;
					shapes[1].transform.position = b;
					shapes[2].transform.position = c;
					shapes[3].transform.position = d;
				}
			}

			//move down fast
			if (Input.GetKey(KeyCode.DownArrow)){
				moveDown();
			}

			//rotate
			if(Input.GetKeyDown(KeyCode.UpArrow)){
				Rotate(shapes[0].transform, shapes[1].transform, shapes[2].transform, shapes[3].transform);
			}
		}

		if(combo >= 2){
			playerOne.GetComponent<Player>().generateRows();
			combo = 0;
		}

		//if nothing spawned, if game over = false, the spawn
		if(!spawn && !gameOver){
			StartCoroutine(Wait());

			spawn = true;

			//Reset roation
			currentRot = 0;
		}

		if(gameOver){
			finishRoundPlayerTwo();
		}
	}

	//wait time before next block spawn
	IEnumerator Wait(){
		yield return new WaitForSeconds(nextBlockSpawnTime);
		SpawnShape(nextShape);

		int random = Random.Range(0,7);

		if(random == nextShape){
			random = Random.Range(0,7);
		}

		//Sshape
		if (random == 0){
			IShape.SetActive(false);
			JShape.SetActive(false);
			LShape.SetActive(false);
			OShape.SetActive(false);
			ZShape.SetActive(false);
			TShape.SetActive(false);

			SShape.SetActive(true);
		}
		//IShape
		else if (random == 1){
			SShape.SetActive(false);
			JShape.SetActive(false);
			LShape.SetActive(false);
			OShape.SetActive(false);
			ZShape.SetActive(false);
			TShape.SetActive(false);

			IShape.SetActive(true);
		}
		//OShape
		else if (random == 2){
			IShape.SetActive(false);
			JShape.SetActive(false);
			LShape.SetActive(false);
			SShape.SetActive(false);
			ZShape.SetActive(false);
			TShape.SetActive(false);

			OShape.SetActive(true);
		}
		//JShape
		else if (random == 3){
			IShape.SetActive(false);
			SShape.SetActive(false);
			LShape.SetActive(false);
			OShape.SetActive(false);
			ZShape.SetActive(false);
			TShape.SetActive(false);

			JShape.SetActive(true);
		}
		//TShape
		else if (random == 4){
			IShape.SetActive(false);
			JShape.SetActive(false);
			LShape.SetActive(false);
			OShape.SetActive(false);
			ZShape.SetActive(false);
			SShape.SetActive(false);

			TShape.SetActive(true);
		}
		//LShape
		else if (random == 5){
			IShape.SetActive(false);
			JShape.SetActive(false);
			SShape.SetActive(false);
			OShape.SetActive(false);
			ZShape.SetActive(false);
			TShape.SetActive(false);

			LShape.SetActive(true);
		}
		//ZShape
		else if (random == 6){
			IShape.SetActive(false);
			JShape.SetActive(false);
			LShape.SetActive(false);
			OShape.SetActive(false);
			SShape.SetActive(false);
			TShape.SetActive(false);

			ZShape.SetActive(true);
		}

		nextShape = random;
	}

	bool CheckMove(Vector3 a, Vector3 b, Vector3 c, Vector3 d){
		//check if we move a block down will it hit something
		if(board[Mathf.RoundToInt(a.x), Mathf.RoundToInt(a.y - 1)] == 1){
			return false;
		}
		if(board[Mathf.RoundToInt(b.x), Mathf.RoundToInt(b.y - 1)] == 1){
			return false;
		}
		if(board[Mathf.RoundToInt(c.x), Mathf.RoundToInt(c.y - 1)] == 1){
			return false;
		}
		if(board[Mathf.RoundToInt(d.x), Mathf.RoundToInt(d.y - 1)] == 1){
			return false;
		}

		return true;
	}

	void moveDown (){
		//spawn blocks positions
		if(shapes.Count != 4){
			return;
		}

		Vector3 a = shapes[0].transform.position;
		Vector3 b = shapes[1].transform.position;
		Vector3 c = shapes[2].transform.position;
		Vector3 d = shapes[3].transform.position;

		//will we hit anything if we move block down
		if(CheckMove(a, b, c, d) == true){
			//move block down by 1
			a = new Vector3(Mathf.RoundToInt(a.x), Mathf.RoundToInt(a.y - 1.0f),a.z);
			b = new Vector3(Mathf.RoundToInt(b.x), Mathf.RoundToInt(b.y - 1.0f),b.z);
			c = new Vector3(Mathf.RoundToInt(c.x), Mathf.RoundToInt(c.y - 1.0f),c.z);
			d = new Vector3(Mathf.RoundToInt(d.x), Mathf.RoundToInt(d.y - 1.0f),d.z);

			pivot.transform.position = new Vector3(pivot.transform.position.x, pivot.transform.position.y - 1, pivot.transform.position.z);

			shapes[0].transform.position = a;
			shapes[1].transform.position = b;
			shapes[2].transform.position = c;
			shapes[3].transform.position = d;
		}
		else {
			//we hit something, stop and mark current shape location as filled in board, also destroy pivot
			Destroy(pivot.gameObject);

			//set ID in board
			board [Mathf.RoundToInt(a.x), Mathf.RoundToInt(a.y)] = 1;
			board [Mathf.RoundToInt(b.x), Mathf.RoundToInt(b.y)] = 1;
			board [Mathf.RoundToInt(c.x), Mathf.RoundToInt(c.y)] = 1;
			board [Mathf.RoundToInt(d.x), Mathf.RoundToInt(d.y)] = 1;

			//check for any match
			checkRow(1);

			//check for gameover
			checkRow(gameOverHeight);

			//clear spawned blocks from array
			shapes.Clear();

			//spawn a few block
			spawn = false;
		}
	}

	bool CheckRotate (Vector3 a, Vector3 b, Vector3 c, Vector3 d){
		//check if block is in board
		if(Mathf.RoundToInt(a.x) < board.GetLength(0) - 1){
			if(board[Mathf.RoundToInt(a.x), Mathf.RoundToInt(a.y)] == 1){
				//if rotated block hit any other block edge, after rotation
				//rotate in default position - previous
				return false;
			}
		}
		//if block isnt in board
		else{
			//not rotate
			return false;
		}

		if(Mathf.RoundToInt(b.x) < board.GetLength(0) - 1){
			if(board[Mathf.RoundToInt(b.x), Mathf.RoundToInt(b.y)] == 1){
				//if rotated block hit any other block edge, after rotation
				//rotate in default position - previous
				return false;
			}
		}
		//if block isnt in board
		else{
			//not rotate
			return false;
		}

		if(Mathf.RoundToInt(c.x) < board.GetLength(0) - 1){
			if(board[Mathf.RoundToInt(c.x), Mathf.RoundToInt(c.y)] == 1){
				//if rotated block hit any other block edge, after rotation
				//rotate in default position - previous
				return false;
			}
		}
		//if block isnt in board
		else{
			//not rotate
			return false;
		}

		if(Mathf.RoundToInt(d.x) < board.GetLength(0) - 1){
			if(board[Mathf.RoundToInt(d.x), Mathf.RoundToInt(d.y)] == 1){
				//if rotated block hit any other block edge, after rotation
				//rotate in default position - previous
				return false;
			}
		}
		//if block isnt in board
		else{
			//not rotate
			return false;
		}

		//we can rotate
		return true;
	}

	void Rotate(Transform a, Transform b, Transform c, Transform d){
		//set parent to pivot so we can rotate
		a.parent = pivot.transform;
		b.parent = pivot.transform;
		c.parent = pivot.transform;
		d.parent = pivot.transform;

		//add rot
		currentRot += 90;
		//reset rot
		if(currentRot == 360){
			currentRot = 0;
		}

		pivot.transform.localEulerAngles = new Vector3(0, 0, currentRot);

		a.parent = null;
		b.parent = null;
		c.parent = null;
		d.parent = null;

		if(CheckRotate(a.position, b.position, c.position, d.position) == false){
			//set parent to pivot so we cna rotate
			a.parent = pivot.transform;
			b.parent = pivot.transform;
			c.parent = pivot.transform;
			d.parent = pivot.transform;

			currentRot -= 90;

			pivot.transform.localEulerAngles = new Vector3(0, 0, currentRot);

			a.parent = null;
			b.parent = null;
			c.parent = null;
			d.parent = null;
		}
	}

	void checkRow(int y){
		//All blocks in the scene
		GameObject[] blocks = GameObject.FindGameObjectsWithTag("BlockTwo");
		//blocks found in a row
		int count = 0;

		//go through each block on this height
		for(int x = 19; x < 29; x++){
			//if there is any block at this position
			if(board[x,y] == 1){
				//we found +1 block
				count ++;
			}
		}

		//if the current height is game over height
		if(y == gameOverHeight && count > 0){
			Debug.Log("GAME OVER");

			gameOver = true;
		}

		//ithe row is full
		if(count == 10){
			//start from bottom of the board(without edge and block spawn space)
			for(int cy = y; cy < board.GetLength(1) - 3; cy++){
				for(int cx = 19; cx < 29; cx++){
					foreach(GameObject go in blocks){
						int height = Mathf.RoundToInt(go.transform.position.y);
						int xPos = Mathf.RoundToInt(go.transform.position.x);

						if(xPos == cx && height == cy){
							//the row we need to destroy
							if(height == y){
								//set empty space
								board[xPos, height] = 0;

								Destroy(go.gameObject);
							}
							else if(height > y){
								//set old position to empty
								board [xPos, height] = 0;

								//set new position
								board[xPos, height - 1] = 1;
								//move block down
								go.transform.position = new Vector3(xPos, height-1, go.transform.position.z);
							}
						}
					}
				}
			}
			//we move blocks down, chack again this row
			checkRow(y);

			lines += 1;
			drawLines();

			score += 10;
			drawScore();

			combo++;
		}

		else if(y + 1 < board.GetLength(1) - 3){
			//check row above this
			checkRow(y + 1);
		}

		checkScore();
	}

	void checkScore(){
		if(score == 200){
			level = 2;
			drawLevel();

			blockFallSpeed = .4f;
			CancelInvoke("moveDown");
			checkFallSpeed();

			if(zenTetrisBool)
				playerOne.GetComponent<AudioSource>().pitch = .8f;

			if(hardTetrisBool)
				playerOne.GetComponent<AudioSource>().pitch = 1.2f;
		}
		else if(score == 800){
			level = 3;
			drawLevel();

			blockFallSpeed = .3f;
			CancelInvoke("moveDown");
			checkFallSpeed();

			if(zenTetrisBool)
				playerOne.GetComponent<AudioSource>().pitch = 1.0f;

			if(hardTetrisBool)
				playerOne.GetComponent<AudioSource>().pitch = 1.4f;
		}
		else if(score == 1600){
			level = 4;
			drawLevel();

			blockFallSpeed = .2f;
			CancelInvoke("moveDown");
			checkFallSpeed();

			if(zenTetrisBool)
				playerOne.GetComponent<AudioSource>().pitch = 1.2f;

			if(hardTetrisBool)
				playerOne.GetComponent<AudioSource>().pitch = 1.6f;
		}
		else if(score == 3200){
			level = 5;
			drawLevel();

			blockFallSpeed = .1f;
			CancelInvoke("moveDown");
			checkFallSpeed();

			if(zenTetrisBool)
				playerOne.GetComponent<AudioSource>().pitch = 1.4f;

			if(hardTetrisBool)
				playerOne.GetComponent<AudioSource>().pitch = 1.8f;
		}

		else if(score == 6400){
			level = 6;
			drawLevel();

			blockFallSpeed = .05f;
			CancelInvoke("moveDown");
			checkFallSpeed();

			if(zenTetrisBool)
				playerOne.GetComponent<AudioSource>().pitch = 1.6f;

			if(hardTetrisBool)
				playerOne.GetComponent<AudioSource>().pitch = 2.0f;
		}
	}

	void drawScore(){
		string scoreStr = score.ToString("00");

		scoreText.text = scoreStr;
	}

	void drawLines(){
		string linesStr = lines.ToString("00");

		linesText.text = linesStr;
	}

	void drawLevel(){
		string levelStr = level.ToString("00");

		levelText.text = levelStr;
	}

	public void finishRoundPlayerTwo(){
		gameOverText.SetActive(true);
		winPlayerOne.SetActive(true);

		//playerOne.GetComponent<Player>().finishRound();

		playerOne.GetComponent<AudioSource>().Stop();

		GameObject[] blocks = GameObject.FindGameObjectsWithTag("BlockTwo");

		foreach(GameObject go in blocks){
			Destroy(go);
		}
	}

	public void zenTetrisMode(){
		zenTetrisBool = true;

		spawn = false;

		//move block down
		checkFallSpeed();

		scoreBacground.SetActive(true);
		levelBackground.SetActive(true);
		linesBacground.SetActive(true);
		nextText.SetActive(true);
	}

	public void hardTetrisMode(){
		hardTetrisBool = true;

		spawn = false;

		//move block down
		checkFallSpeed();

		scoreBacground.SetActive(true);
		levelBackground.SetActive(true);
		linesBacground.SetActive(true);
		nextText.SetActive(true);
	}

	#region
	Transform GenRockBlock (Vector3 pos){

		Transform obj = (Transform)Instantiate(RockBlock.transform, pos, Quaternion.identity) as Transform; 
		obj.tag = "BlockTwo";

		return obj;
	} 

	public void generateRowsTwo(){
		//Debug.Log("GENERAAAAAA");

		int height = board.GetLength(1) - 6;
		int randomPos = Random.Range(20,30);
		int randomShape = Random.Range(0,3);

		int xPos = randomPos;

		pivotRow = new GameObject("RotateAround");

		if(randomShape == 0){
			pivotRow.transform.position = new Vector3 (xPos + 0.5f, height, 0);

			rockShapes.Add(GenRockBlock(new Vector3(xPos, height, 0)));
			rockShapes.Add(GenRockBlock(new Vector3(xPos + 1, height, 0)));
			rockShapes.Add(GenRockBlock(new Vector3(xPos, height - 1, 0)));

			//Debug.Log("CREADO L");
		}
		else if(randomShape == 1){
			pivotRow.transform.position = new Vector3 (xPos + 0.5f, height, 0);

			rockShapes.Add(GenRockBlock(new Vector3(xPos, height, 0)));
			rockShapes.Add(GenRockBlock(new Vector3(xPos - 1, height, 0)));
			rockShapes.Add(GenRockBlock(new Vector3(xPos, height + 1, 0)));

			//Debug.Log("CREADO L");
		}

		InvokeRepeating("moveDownRockBlocks", .1f, .1f);
	}

	bool CheckRockMove(Vector3 a, Vector3 b, Vector3 c){
		//check if we move a block down will it hit something
		if(board[Mathf.RoundToInt(a.x), Mathf.RoundToInt(a.y - 1)] == 1){
			return false;
		}
		if(board[Mathf.RoundToInt(b.x), Mathf.RoundToInt(b.y - 1)] == 1){
			return false;
		}
		if(board[Mathf.RoundToInt(c.x), Mathf.RoundToInt(c.y - 1)] == 1){
			return false;
		}

		return true;
	}

	void moveDownRockBlocks(){
		//spawn blocks positions
		if(rockShapes.Count != 3){
			return;
		}
			
		Vector3 a = rockShapes[0].transform.position;
		Vector3 b = rockShapes[1].transform.position;
		Vector3 c = rockShapes[2].transform.position;

		//will we hit anything if we move block down
		if(CheckRockMove(a, b, c) == true){
			//move block down by 1
			a = new Vector3(Mathf.RoundToInt(a.x), Mathf.RoundToInt(a.y - 1.0f),a.z);
			b = new Vector3(Mathf.RoundToInt(b.x), Mathf.RoundToInt(b.y - 1.0f),b.z);
			c = new Vector3(Mathf.RoundToInt(c.x), Mathf.RoundToInt(c.y - 1.0f),c.z);

			pivotRow.transform.position = new Vector3(pivotRow.transform.position.x, pivotRow.transform.position.y - 1, pivotRow.transform.position.z);

			rockShapes[0].transform.position = a;
			rockShapes[1].transform.position = b;
			rockShapes[2].transform.position = c;
		}
		else {
			//we hit something, stop and mark current shape location as filled in board, also destroy pivot
			Destroy(pivotRow.gameObject);

			//set ID in board
			board [Mathf.RoundToInt(a.x), Mathf.RoundToInt(a.y)] = 1;
			board [Mathf.RoundToInt(b.x), Mathf.RoundToInt(b.y)] = 1;
			board [Mathf.RoundToInt(c.x), Mathf.RoundToInt(c.y)] = 1;

			//check for any match
			checkRow(1);

			//check for gameover
			checkRow(gameOverHeight);

			//clear spawned blocks from array
			rockShapes.Clear();
		}
	}
	#endregion
}