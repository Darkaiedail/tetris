﻿using UnityEngine;
using System.Collections;

public class LShapeMovement : MonoBehaviour {

	Vector3 rot = new Vector3(-180,0,0);

	void Update () {
		iTween.RotateAdd(this.gameObject,iTween.Hash("amount", rot,
													 "time", 2.0f,
													 "easetype","spring"));
	}
}
