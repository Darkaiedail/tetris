﻿using UnityEngine;
using System.Collections;

public class ShapeMovement : MonoBehaviour {

	Vector3 rot = new Vector3(0,180,0);

	void Update () {
		iTween.RotateAdd(this.gameObject,iTween.Hash("amount", rot,
													 "time", 2.0f,
												   	 "easetype","spring"));
	}
}
